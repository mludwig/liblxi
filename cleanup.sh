#!/bin/bash
rm -rf ./CMakeFiles
find ./ -name "cmake_*.cmake" -exec rm -rf {} \;
find ./ -name "Makefile" -exec rm -rvf {} \;
find ./ -name "CMakeCache.txt" -exec rm -rvf {} \;
find ./ -name "CMakeFiles" -exec rm -rvf {} \;
find ./ -name "*.o" -exec rm -rvf {} \;
#


